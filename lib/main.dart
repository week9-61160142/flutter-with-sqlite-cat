import 'package:flutter/material.dart';
import 'cat.dart';
import 'cat_dao.dart';

void main() async {
  var tam = Cat(
    id: 0,
    name: 'Tam',
    age: 21,
  );
  var boon = Cat(
    id: 1,
    name: 'Boon',
    age: 15,
  );
  var dum = Cat(
    id: 1,
    name: 'Dum',
    age: 12,
  );
  await CatDao.insertCat(tam);
  await CatDao.insertCat(boon);
  await CatDao.insertCat(dum);

  print(await CatDao.cats());

  tam = Cat(
    id: tam.id,
    name: tam.name,
    age: tam.age + 7,
  );

  await CatDao.updateCat(tam);
  print(await CatDao.cats());

  await CatDao.deleteCat(0);
  print(await CatDao.cats());
}
